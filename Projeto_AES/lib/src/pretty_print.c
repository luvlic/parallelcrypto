#include <stdio.h>
#include <wmmintrin.h>
#include "../include/aes.h"

void print_m128i_with_string(char* string,__m128i data)
{
        unsigned char *pointer = (unsigned char*)&data;

        printf("%-40s[0x",string);
        for (int i = 0; i < 16; i++)
                printf("%02x",pointer[i]);
        printf("]\n");
}

void print_m128i_with_string_short(char* string,__m128i data,int length)
{
        unsigned char *pointer = (unsigned char*)&data;

        printf("%-40s[0x",string);
        for (int i = 0; i < length; i++)
                printf("%02x",pointer[i]);
        printf("]\n");
}
