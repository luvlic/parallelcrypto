#include <stdio.h>
#include <string.h>
#include <wmmintrin.h>
#include "../include/cbc_main.h"
#include "../include/ecb_main.h"
#include "../include/ctr_main.h"
#include "../include/aes.h"

int main(int argc, char const *argv[])
{
        if(argc != 3) {
                printf("Número de argumentos inválido!\n");
                printf("Formato: ./aes_main [ECB|CBC|CTR] [128|192|256]\n");
                exit(EXIT_FAILURE);
        }

        int size = atoi(argv[2]);
        if(size  != 128 && size != 192 && size != 256) {
                printf("Segundo argumento inválido!\n");
                printf("Formato: ./aes_main [ECB|CBC|CTR] [128|192|256]\n");
                exit(EXIT_FAILURE);
        }

        if(strcmp(argv[1], "ECB") == 0) {
                ecb_main(size);
        } else if(strcmp(argv[1], "CBC") == 0) {
                cbc_main(size);
        } else if(strcmp(argv[1], "CTR") == 0) {
                ctr_main(size);
        } else {
                printf("Primeiro argumento inválido!\n");
                printf("Formato: ./aes_main [ECB|CBC|CTR] [128|192|256]\n");
                exit(EXIT_FAILURE);
        }

        return 0;
}
