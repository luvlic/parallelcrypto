#ifndef PRETTY_PRINT_H
#define	PRETTY_PRINT_H

#include <wmmintrin.h>

void print_m128i_with_string(char* string,__m128i data);
void print_m128i_with_string_short(char* string,__m128i data,int length);

#endif
