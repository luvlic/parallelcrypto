library(data.table)
results = add.response(design = "MyFirstDesign", response = "results/aes_ms_05-17-2016-220733.csv", rdapath = "MyFirstDesign.rda")
myDT <- data.table(results)
variables <- tail( names(myDT), -2)
mean_results    = myDT[, lapply(.SD, mean),  .SDcols=variables, by=list(Procs, Size)]
var_results     = myDT[, lapply(.SD, sd), .SDcols=variables, by=list(Procs, Size)]
mean_results[, "std"] <- var_results$time
mean_results[, "error"] <- qt(0.975,df=4)*(mean_results$std)/sqrt(5)
ggplot(mean_results, aes(x=Size, y=time, colour=Procs)) +
geom_errorbar(aes(ymin=time-error, ymax=time+error), width=.1) +
geom_point() + facet_wrap( ~ Procs)
