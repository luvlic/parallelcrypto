// AES usage example
// compile as: mpigcc crackms2.c -o crackms2
// execute as: mpirun -np 8 crackms2 2

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "../include/aes.h"
#include "mpi.h"

// AES Code: Chris Hulbert - chris.hulbert@gmail.com - http://splinter.com.au/blog
// This code is public domain, or any OSI-approved license, your choice. No warranty.

extern void AES_ECB_encrypt       (const unsigned char *in,
                                   unsigned char *out,
                                   unsigned long length,
                                   const unsigned char *KS,
                                   int nr);
extern void AES_ECB_decrypt       (const unsigned char *in,
                                   unsigned char *out,
                                   unsigned long length,
                                   const unsigned char *KS,
                                   int nr);

typedef unsigned char byte;
#define cpuid(func,ax,bx,cx,dx) \
        __asm__ __volatile__ ("cpuid" : \
                              "=a" (ax), "=b" (bx), "=c" (cx), "=d" (dx) : "a" (func));

// Pretty-print a key (or any smallish buffer) onto screen as hex
static void Pretty(const byte* b,int len,char* label) {
        char out[100];
        int i;
        for (i=0; i<len; i++) {
                sprintf(out+i*2,"%02x",b[i]);
        }
        printf("%s%s\r\n",label, out);
}

byte symbol[62] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
byte basekey[16] = "superstrongerkey";
byte plaintext[16] = "Attack at dawn!!";
#define ARRAY_SIZE 18
#define SENPUKU_TAG -1

int my_rank;
int proc_n;
MPI_Status status;
int SLAVES;
int range_size;
int work[ARRAY_SIZE];  // para enviar e receber dados entre mestre e escravo (lower limit, upper limit, ciphertext)

int Check_CPU_support_AES() {
        unsigned int a,b,c,d;
        cpuid(1, a,b,c,d);
        return (c & 0x2000000);
}

int crack(byte *ptext, byte *ctext, int li, int ls){
        byte key[16];
        byte decptext[16];
        int i15,i14,i13,i12,i11;
        int i;
        int found;
        found = 0;
        AES_KEY decrypt_key;
        for (i11=li; i11<ls; i11++) {
                for (i=0; i<16; i++) key[i]=basekey[i];
                key[11]=symbol[i11];
                printf("Testing with key[11]=%c...\n",key[11]);
                for (i12=0; i12<62; i12++) {
                        key[12]=symbol[i12];
                        for (i13=0; i13<62; i13++) {
                                key[13]=symbol[i13];
                                for (i14=0; i14<62; i14++) {
                                        key[14]=symbol[i14];
                                        for (i15=0; i15<62; i15++) {
                                                key[15]=symbol[i15];
                                                AES_set_decrypt_key(key, 128, &decrypt_key);
                                                AES_ECB_decrypt(ctext, decptext, 16, decrypt_key.KEY, decrypt_key.nr);
                                                if (strncmp((const char *)decptext,(const char *)ptext,16)==0) {
                                                        printf("%d: Key found: %.16s; Decrypted text: %.16s\n",my_rank,key,decptext);
                                                        found = 1;
                                                }
                                        } // for i15
                                } // for i14
                        } // for i13
                } // for i12
        }  // for i11
        return found;
}

//Funcao para o escravo
void slave_func() {
        int trabalho = 1;
        byte ciphertext[16];
        int j;

        while (trabalho > 0) {
                //Aguarda o recebimento dos trabalhos
                MPI_Recv(work, ARRAY_SIZE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                // Se o primeiro elemento é negativo, não há mais nada a fazer
                if (work[0] < 0) {
                        trabalho = -1;
//            printf("Escravo %d encerrando trabalho.\n",my_rank);
                }
                else {
                        // Realiza o trabalho (pesquisa na faixa) e envia ao mestre.
//                printf("Escravo %d recebeu trabalho na faixa %d-%d\n",my_rank,work[0],work[1]-1);
                        for (j=2; j < ARRAY_SIZE; j++) ciphertext[j-2] = (byte) work[j];
                        work[0] = crack(plaintext,ciphertext,work[0],work[1]);
                        MPI_Send(work, ARRAY_SIZE, MPI_INT, 0, 1, MPI_COMM_WORLD);
                }
        }
}

//Função para o mestre
void master_func() {
        int sl = SLAVES;
        int range = range_size;
        int lower=0;
        int upper=61;
        int i;
        int j;
        int index;
        int srank;
        double start; //tempo de comeco do trabalho
        double end; //tempo de finalizacao do trabalho
        byte ciphertext[16];

        printf ("Key cracking tests.\n");
        printf ("Number of slaves available = %d\n", SLAVES);

        AES_KEY keys;
        AES_set_encrypt_key(basekey, 128, &keys);
        AES_ECB_encrypt(plaintext, ciphertext, 16, keys.KEY, keys.nr);

        start = MPI_Wtime(); //tempo de inicio do trabalho.

        index=lower;
        // manda um vetor para cada escravo
        for(i = 1; i <= SLAVES; i++) {
                work[0]=index;
                index=index+range;
                work[1]=index;
                if (index > upper) work[1] = upper+1;
                for (j=2; j < ARRAY_SIZE; j++) work[j] = (int) ciphertext[j-2];
//                printf("Mestre enviou trabalho para o escravo %d na faixa %d-%d.\n",i,work[0],work[1]-1);
                MPI_Send(work, ARRAY_SIZE, MPI_INT, i, 1, MPI_COMM_WORLD);
        }
        // espera receber dos escravos e manda novos vetores até o tamanho do saco
        sl = SLAVES;  // cada escravo está trabalhando
        while (index <= upper) {
                // recebeu um vetor
                MPI_Recv(work, ARRAY_SIZE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                srank = status.MPI_SOURCE;
                if (work[0]==1) printf("Mestre recebeu chave do escravo %d.\n",srank);
                if (index <= upper) {
                        // ainda tem faixas a pesquisar
                        work[0]=index;
                        index=index+range;
                        work[1]=index;
                        if (index > upper) work[1] = upper+1;
                        for (j=2; j < ARRAY_SIZE; j++) work[j] = (int) ciphertext[j-2];
//                printf("Mestre enviou novo trabalho para o escravo %d na faixa %d-%d.\n",srank,work[0],work[1]-1);
                        MPI_Send(work, ARRAY_SIZE, MPI_INT, srank, 1, MPI_COMM_WORLD);
                }
                else {
                        // terminou. Não há mais trabalhos a enviar. Libera o escravo
                        work[0]=-1;
                        MPI_Send(work, ARRAY_SIZE, MPI_INT, srank, 1, MPI_COMM_WORLD);
                        sl--;
                }
        }
        //Espera receber dos escravos que faltam.
        while (sl >0) {
                MPI_Recv(work, ARRAY_SIZE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                srank = status.MPI_SOURCE;
                // printf("Mestre recebeu trabalho do escravo %d.\n",srank);
                work[0] = -1;
                MPI_Send(work, ARRAY_SIZE, MPI_INT, srank, 1, MPI_COMM_WORLD);
                sl--;
        }

        end = MPI_Wtime(); //tempo de fim do trabalho
        //Imprime o tempo total do trabalho.
        printf("Tempo total do trabalho: [%1.4f]\n", end-start);
        fflush(stdout);
}

int main(int argc, char** argv) {
        MPI_Init(&argc, &argv);
        MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
        MPI_Comm_size(MPI_COMM_WORLD, &proc_n);

        if (!Check_CPU_support_AES()) {
                printf("Cpu does not support AES instruction set. Bailing out.\n");
                return 1;
        } else {
                printf("CPU support AES instruction set.\n\n");
        }

        /* Parse command line arguments */
        if(argc != 2) {
                printf("Error! Usage: %s <search_range>.\n", argv[0]);
                exit(0);
        }
        else {
                SLAVES = proc_n - 1;
                range_size = atoi(argv[1]);
        }

        //rank = 0 corresponde ao mestre; rank != corresponde aos escravos.
        if(my_rank == 0)
                master_func();
        else
                slave_func();
        MPI_Finalize();
        return 0;
}
