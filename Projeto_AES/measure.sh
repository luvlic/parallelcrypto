#!/bin/bash

SCRIPT_PATH="$(cd "$(dirname "$0")" && pwd)"
cd "$SCRIPT_PATH" || exit 1

TIMEFORMAT='%3R'
vertfonce='\e[0;32m'
neutre='\e[0;m'

NOW=$(date +"%m-%d-%Y-%H%M%S")
FILE_RES="./results/"$(basename $1)"_"$NOW".csv"

START=1
END=$2

function ProgressBar {
        # Process data
        let _progress=(${1}*100/${2}*100)/100
        let _done=(${_progress}*4)/10
        let _left=40-$_done
        # Build progressbar string lengths
        _fill=$(printf "%${_done}s")
        _empty=$(printf "%${_left}s")

        # 1.2 Build progressbar strings and print the ProgressBar line
        # 1.2.1 Output example:
        # 1.2.1.1 Progress : [########################################] 100%
        printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"

}

echo "Starting tests"

for ((i=START; i<=END; i++)); do
        (time $1 > /dev/null) |& tr -d '\n' |& tr '.' ',' &>> $FILE_RES
        echo -ne "\n" >> $FILE_RES
        ProgressBar "$i" "$END"
done
echo ""
echo -e "${vertfonce}[OK] Results generated${neutre}"
