#!/bin/bash

SCRIPT_PATH="$(cd "$(dirname "$0")" && pwd)"
cd "$SCRIPT_PATH" || exit 1

TIMEFORMAT='%3R'
vertfonce='\e[0;32m'
neutre='\e[0;m'

NOW=$(date +"%m-%d-%Y-%H%M%S")
FILE_RES="../results/aes_ms_"$NOW".csv"
TEMPLATE=$2
BIN= $1

function ProgressBar {
        # Process data
        let _progress=(${1}*100/${2}*100)/100
        let _done=(${_progress}*4)/10
        let _left=40-$_done
        # Build progressbar string lengths
        _fill=$(printf "%${_done}s")
        _empty=$(printf "%${_left}s")

        # 1.2 Build progressbar strings and print the ProgressBar line
        # 1.2.1 Output example:
        # 1.2.1.1 Progress : [########################################] 100%
        printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"

}

if [ ! -f $TEMPLATE ]; then
    echo "$TEMPLATE not found!"
fi

if [ ! -f $BIN ]; then
    echo "$BIN not found!"
fi

echo "Starting tests"
sed -n '1{p;q}' $TEMPLATE > $FILE_RES

NB_LINE=$(wc -l < $TEMPLATE)

for ((i=2; i<=NB_LINE; i++)); do
    LINE=$(sed -n "$i{p;q}" $TEMPLATE)
    LINE=$(echo $LINE | cut -f1-6 -d",")
	echo -ne "$LINE,\"" >> $FILE_RES
    PROC=$(echo $LINE | cut -f5 -d"," | sed -e 's/\"//g')
    SIZE=$(echo $LINE | cut -f6 -d"," | sed -e 's/\"//g')
	(time mpirun -np $PROC $1 $SIZE > /dev/null) |& tr -d '\n' |& tr '.' ',' &>> $FILE_RES
	echo -ne "\"\n" >> $FILE_RES
	ProgressBar "$i" "$NB_LINE"
done
echo ""
echo -e "${vertfonce}[OK] Results generated${neutre}"
