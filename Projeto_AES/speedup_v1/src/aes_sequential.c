#include <stdio.h>
#include <time.h>
#include <string.h>
#include "../include/aes.h"

extern void AES_ECB_encrypt       (const unsigned char *in,
                                   unsigned char *out,
                                   unsigned long length,
                                   const unsigned char *KS,
                                   int nr);
extern void AES_ECB_decrypt       (const unsigned char *in,
                                   unsigned char *out,
                                   unsigned long length,
                                   const unsigned char *KS,
                                   int nr);

typedef unsigned char byte;
#define cpuid(func,ax,bx,cx,dx) \
        __asm__ __volatile__ ("cpuid" : \
                              "=a" (ax), "=b" (bx), "=c" (cx), "=d" (dx) : "a" (func));

byte basekey[16] = "superstrongerkey";
byte plaintext[16] = "Attack at dawn!!";

int Check_CPU_support_AES() {
        unsigned int a,b,c,d;
        cpuid(1, a,b,c,d);
        return (c & 0x2000000);
}

int crack(byte *ptext,
          byte *ctext, byte li, byte ls){
        byte key[16];
        byte decptext[16];
        char c15,c14,c13,c12;
        AES_KEY decrypt_key;
        int i;
        int found;
        found = 0;

        for (c12=(char)li; c12<(char)ls; c12++) {
                for (i=0; i<16; i++) key[i]=basekey[i];
                key[12]=c12;
                printf("Testing with key[12]=%c...\n",key[12]);
                for (c13=(char)33; c13<(char)127; c13++) {
                        key[13]=c13;
                        for (c14=(char)33; c14<(char)127; c14++) {
                                key[14]=c14;
                                for (c15=(char)33; c15<(char)127; c15++) {
                                        key[15]=c15;
                                        AES_set_decrypt_key(key, 128, &decrypt_key);
                                        AES_ECB_decrypt(ctext, decptext, 16, decrypt_key.KEY, decrypt_key.nr);
                                        if (strncmp((const char *)decptext, (const char *)ptext,10)==0) {
                                                printf("Key found: %.16s; Decrypted text: %.16s\n",key,decptext);
                                                found = 1;
                                        }
                                } // for i15
                        } // for i14
                } // for i13
        }  // for i12
//	}  // for i11
        return found;
}

int main(int argc, char *argv[]){
        if (!Check_CPU_support_AES()) {
                printf("Cpu does not support AES instruction set. Bailing out.\n");
                return 1;
        } else {
                printf("CPU support AES instruction set.\n\n");
        }

        clock_t start, end;
        byte ciphertext[16];

        printf ("Key cracking tests. Scanning from ASCII 33 (!) to ASCII 126 (~).\n");
        printf ("WITH version.\n");

        AES_KEY keys;

        AES_set_encrypt_key(basekey, 128, &keys);
        AES_ECB_encrypt(plaintext, ciphertext, 16, keys.KEY, keys.nr);

        start = clock();
        crack(plaintext,ciphertext,33,127);
        end = clock();

        printf( "Time in seconds: %f\n", (end-start)/(double)CLOCKS_PER_SEC );
        printf ( "End of execution.\n" );
        return 0;
}
