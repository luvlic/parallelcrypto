#ifndef DECRYPT_WITHOUT_INST_H
#define	DECRYPT_WITHOUT_INST_H

typedef unsigned char byte;

void decryptAES_Whithout_Instruction(const byte *c, const byte *key, byte *m);

#endif
