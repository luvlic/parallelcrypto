#ifndef ENCRYPT_WITHOUT_INST_H
#define	ENCRYPT_WITHOUT_INST_H

typedef unsigned char byte;

void expand_key(const byte *key, byte *keys);
void EncryptAES_Whithout_Instruction(const byte *msg, const byte *key, byte *c);

#endif
