#ifndef ERROR_H
#define	ERROR_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define NRM  "\x1B[0m"
#define RED  "\x1B[31m"
#define GRN  "\x1B[32m"
#define BLU  "\x1B[34m"
#define YEL  "\x1B[33m"

static void exitError(const char* msg) {
        fprintf(stderr, RED "[Error] %s\n" NRM, msg);
        exit(1);
}

static void printError(const char* msg) {
        fprintf(stderr, RED "[Error] %s\n" NRM, msg);
}

static void fileExist(const char* path) {
        if((path != NULL) && (access(path, R_OK) == -1)) {
                char str[strlen(path) + 20];
                sprintf(str, "File \"%s\" doesn't exist.", path);
                exitError(str);
        }
}

#endif
