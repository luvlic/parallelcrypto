// Copyright (c) 2016 Ludovic Boulay
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LIST_H
#define	LIST_H

typedef enum {VERY_CRITICAL, CRITICAL, CAREFUL, GOOD, NO_DEF} criticity;

struct cell {
        char            *username;
        char            *salt;
        char            *hash;
        char            *password;
        criticity       criticity_lvl;
        struct cell     *next;
};

struct list {
        struct cell     *head;
        struct cell     *tail;
        int             nb_cell;
};

struct list *new_list();
int insert_cell(struct list *l, char *u, char* s, char * h, char * pwd, criticity c);
void save_list(char* path);
struct list *load_list(char *path);
void print_list(struct list *list);
void destroy_list(struct list *l);

#endif
