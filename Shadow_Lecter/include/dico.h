// Copyright (c) 2016 Ludovic Boulay
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DICO_H
#define	DICO_H

struct list_dico {
        char                    *path;
        int                     nb_words;
        char                    hash[33];
        struct list_dico        *next;
};

struct list_dico *init_dictionaries(const char *path);

struct list_dico *list_file (const char *dir);
void init_params(struct list_dico *l);
int check_dico(char *path, char *hash);

#endif
