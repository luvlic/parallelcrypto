#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/list.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"

int EXPECTED(const char *c1, const char *c2)
{
        if(c1 == NULL) {
                printf("%s[NOK]%s Expected %s but received NULL\n", KRED, KNRM,c1);
                return 1;
        }

        if(strcmp(c1, c2) == 0) {
                printf("%s[OK]%s %s\n", KGRN, KNRM, c1);
                return 0;
        } else {
                printf("%s[NOK]%s Expected %s but received %s\n", KRED, KNRM, c2, c1);
                return 1;
        }
}

int main() {
        struct list *list_1 = new_list();

        char user[100], salt[100], hash[100], pass[100];
        memset(user, '\0', sizeof(user));
        memset(salt, '\0', sizeof(salt));
        memset(hash, '\0', sizeof(hash));
        memset(pass, '\0', sizeof(pass));

        strcpy(user, "Ludovic");
        strcpy(salt, "salt");
        strcpy(hash, "hash");
        insert_cell(list_1, user, salt, hash, NULL, GOOD);

        strcpy(user, "Ludovi");
        strcpy(salt, "salt");
        strcpy(hash, "hash");
        insert_cell(list_1, user, salt, hash, NULL, VERY_CRITICAL);

        strcpy(user, "Lucas");
        strcpy(salt, "");
        strcpy(hash, "hash");
        insert_cell(list_1, user, salt, hash, NULL, CRITICAL);

        strcpy(user, "");
        strcpy(salt, "salt");
        strcpy(hash, "");
        strcpy(pass, "banane");
        insert_cell(list_1, user, salt, hash, pass, CAREFUL);

        strcpy(user, "Ludovic");
        strcpy(salt, "$6$4hTy71HC");
        strcpy(hash, "2bemwMgD9AEbRjPeudJ8Pj.xkT6ofRppv09q5eBt9vEyxk7BmxiTGQMnYOVkdTGwANcDOItkFI2559hPm5KJU/");
        insert_cell(list_1, user, salt, hash, NULL, GOOD);

        if(list_1->nb_cell == 5) {
            printf("%s[OK]%s All 5 cells inserted.\n", KGRN, KNRM);
        } else {
            printf("%s[NOK]%s All 5 cells not well inserted.\n", KRED, KNRM);
        }

        struct cell *tmp = list_1->head;

        EXPECTED(tmp->username, "Ludovic");
        EXPECTED(tmp->salt, "salt");
        EXPECTED(tmp->hash, "hash");
        tmp = tmp->next;

        EXPECTED(tmp->username, "Ludovi");
        EXPECTED(tmp->salt, "salt");
        EXPECTED(tmp->hash, "hash");
        tmp = tmp->next;

        EXPECTED(tmp->username, "Lucas");
        EXPECTED(tmp->salt, "");
        EXPECTED(tmp->hash, "hash");
        tmp = tmp->next;

        EXPECTED(tmp->username, "");
        EXPECTED(tmp->salt, "salt");
        EXPECTED(tmp->hash, "");
        EXPECTED(tmp->password, "banane");
        tmp = tmp->next;

        EXPECTED(tmp->username, "Ludovic");
        EXPECTED(tmp->salt, "$6$4hTy71HC");
        EXPECTED(tmp->hash, "2bemwMgD9AEbRjPeudJ8Pj.xkT6ofRppv09q5eBt9vEyxk7BmxiTGQMnYOVkdTGwANcDOItkFI2559hPm5KJU/");
        tmp = list_1->tail;

        EXPECTED(tmp->hash, "2bemwMgD9AEbRjPeudJ8Pj.xkT6ofRppv09q5eBt9vEyxk7BmxiTGQMnYOVkdTGwANcDOItkFI2559hPm5KJU/");

        destroy_list(list_1);

        return 0;
}
