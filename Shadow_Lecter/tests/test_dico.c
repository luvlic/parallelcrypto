#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/dico.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"

int EXPECTED(const char *c1, const char *c2)
{
        if(c1 == NULL) {
                printf("%s[NOK]%s\tExpected %s but received NULL\n", KRED, KNRM,c2);
                return 1;
        }

        if(strcmp(c1, c2) == 0) {
                printf("%s[OK]%s\t%s\n", KGRN, KNRM, c1);
                return 0;
        } else {
                printf("%s[NOK]%s\tExpected %s but received %s\n", KRED, KNRM, c2, c1);
                return 1;
        }
}

int EXPECTED2(int d1, int d2)
{
        if(d1 == d2) {
                printf("%s[OK]%s\t%d\n", KGRN, KNRM, d1);
                return 0;
        } else {
                printf("%s[NOK]%s\tExpected %d but received %d\n", KRED, KNRM, d2, d1);
                return 1;
        }
}

int main() {
        struct list_dico *l = list_file("./tests/FILE_DICO_TESTS/");
        struct list_dico *tmp = l;

        if(tmp != NULL) {
                EXPECTED(tmp->path, "./tests/FILE_DICO_TESTS/test3.txt");
                tmp = tmp->next;
        } else printf("%s[NOK]%s\tExpected a dico but received NULL\n", KRED, KNRM);
        if(tmp != NULL) {
                EXPECTED(tmp->path, "./tests/FILE_DICO_TESTS/test2.txt");
                tmp = tmp->next;
        } else printf("%s[NOK]%s\tExpected a dico but received NULL\n", KRED, KNRM);
        if(tmp != NULL) {
                EXPECTED(tmp->path, "./tests/FILE_DICO_TESTS/test1.txt");
                tmp = tmp->next;
        } else printf("%s[NOK]%s\tExpected a dico but received NULL\n", KRED, KNRM);

        tmp = l;
        init_params(l);

        if(tmp != NULL) {
                EXPECTED(tmp->hash, "709c2ad238f77728a6183eb1b365e4b3");
                EXPECTED2(tmp->nb_words, 800);
                tmp = tmp->next;
        } else printf("%s[NOK]%s\tExpected a dico but received NULL\n", KRED, KNRM);
        if(tmp != NULL) {
                EXPECTED(tmp->hash, "1eb8d7a7da33d3c20689a7f0c1cc839a");
                EXPECTED2(tmp->nb_words, 80);
                tmp = tmp->next;
        } else printf("%s[NOK]%s\tExpected a dico but received NULL\n", KRED, KNRM);
        if(tmp != NULL) {
                EXPECTED(tmp->hash, "e47de42104dcd6988630e55c5e0eae17");
                EXPECTED2(tmp->nb_words, 5);
                tmp = tmp->next;
        } else printf("%s[NOK]%s\tExpected a dico but received NULL\n", KRED, KNRM);

        return 0;
}
