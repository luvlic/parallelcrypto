#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/parser.h"
#include "../include/list.h"

int main() {
    struct list *list= shadow_parser("shadow/shadow_1.txt");
    print_list(list);
    destroy_list(list);
    return 0;
}
