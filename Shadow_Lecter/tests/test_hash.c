#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/hash.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"

static char *test[] = {
        "",
        "a",
        "abc",
        "message digest",
        "abcdefghijklmnopqrstuvwxyz",
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
        "12345678901234567890123456789012345678901234567890123456789012345678901234567890",
        "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
        NULL,
};

static char *salt[] = {
        "",
        "$6$QR3drPrQ$",
        NULL,
};

static char *expected[] = {
};

int EXPECTED(const char *c1, const char *c2)
{
        if(c1 == NULL) {
                printf("%s[NOK] Expected %s but received NULL\n", KRED, c1);
                return 1;
        }

        if(strcmp(c1, c2) == 0) {
                printf("%s[OK] %s\n", KGRN, c1);
                return 0;
        } else {
                printf("%s[NOK] Expected %s but received %s\n", KRED, c2, c1);
                return 1;
        }
}


int main()
{
        char *res = NULL;
        char **P, **R, **S;

        printf("[Begin Tests]\n");

        P = test;
        R = expected;
        while (*P != NULL) {
                S = salt;
                while(*S != NULL) {
                        res = hash(*P, *S);
                        EXPECTED(res, *R);
                        free(res);
                        S++;
                        R++;
                }
                P++;
        }

        printf("%s[End Tests]\n", KNRM);

        return 0;

}
