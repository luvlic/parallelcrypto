#!/bin/bash

NBPROC=$1
SHADOW=$2

/home/ludovic/.openmpi/bin/mpirun -np $NBPROC --hostfile host_file --mca btl_tcp_if_exclude virbr0 bin/shadow_lecter shadow/shadow_$SHADOW.txt
