#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include "../include/dico.h"
#include "../include/error.h"


#define STR_VALUE(val) #val
#define STR(name) STR_VALUE(name)

#define PATH_LEN 256
#define MD5_LEN 32

struct list_dico *list_file (const char *dir)
{
        DIR *dp;
        struct dirent *ep;
        struct list_dico *l = NULL;

        dp = opendir (dir);
        if (dp != NULL) {
                while((ep = readdir(dp)) != NULL) {
                        if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
                                struct list_dico *tmp = (struct list_dico *)calloc(1, sizeof(struct list_dico));
                                tmp->next       = l;
                                l               = tmp;
                                char buf[10 + strlen(ep->d_name)];
                                sprintf(buf, "%s%s", dir, ep->d_name);
                                l->path         = strdup(buf);
                        }
                }
                (void) closedir (dp);
        }
        else
                perror ("Couldn't open the directory");

        return l;
}

int CalcFileMD5(char *file_name, char *md5_sum)
{
    #define MD5SUM_CMD_FMT "md5sum %." STR(PATH_LEN) "s 2>/dev/null"
    char cmd[PATH_LEN + sizeof (MD5SUM_CMD_FMT)];
    sprintf(cmd, MD5SUM_CMD_FMT, file_name);
    #undef MD5SUM_CMD_FMT

    FILE *p = popen(cmd, "r");
    if (p == NULL) return 0;

    int i, ch;
    for (i = 0; i < MD5_LEN && isxdigit(ch = fgetc(p)); i++) {
        *md5_sum++ = ch;
    }

    *md5_sum = '\0';
    pclose(p);
    return i == MD5_LEN;
}

void init_params(struct list_dico *l)
{
        struct list_dico *tmp = l;
        while(tmp != NULL) {
                FILE * dico = fopen(tmp->path, "r");
                int car, NbLigne = 0;
                while (( car = fgetc(dico)) != EOF)
                        if (car == '\n') ++NbLigne;
                tmp->nb_words = NbLigne;
                fclose(dico);
                if (!CalcFileMD5(tmp->path, tmp->hash)) {
                        exitError("Error: Md5 error.\n");
                }

                tmp = tmp->next;
        }
}

struct list_dico *init_dictionaries(const char *path)
{
        struct list_dico *l = list_file(path);
        init_params(l);
        return l;
}

int check_dico(char *path, char *hash)
{
    char hash_calc[33];
    CalcFileMD5(path, hash_calc);
    for (size_t i = 0; i < 33; i++) {
        if(hash_calc[i] != hash[i]) return 0;
    }
    return 1;
}
