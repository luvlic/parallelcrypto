#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <regex.h>
#include "../include/list.h"
#include "../include/error.h"

struct list *shadow_parser(const char *path)
{
        fileExist(path);

        FILE *shadow_file = fopen(path, "r");
        if (!shadow_file)
                exitError("Error opening the shadow file for reading.\n");

        char user[100], salt[100], hash[100], buf[300];
        memset(user, '\0', sizeof(user));
        memset(salt, '\0', sizeof(salt));
        memset(hash, '\0', sizeof(hash));
        memset(buf,  '\0', sizeof(buf));

        char *token, *begin_salt, *end_salt;
        struct list *list = new_list();
        size_t  index;
        regex_t regex;
        int ret;

        // :([$][1,2,2a,5,6][$](.*)|!|[*]):(.*):(.*):(.*):(.*):(.*):(.*):(.*)
        ret = regcomp(&regex, "^(.*):([$][1,2,2a,5,6][$](.*)[$](.*)|!|[*]):(.*):(.*):(.*):(.*):(.*):(.*):(.*)", REG_NOSUB | REG_EXTENDED);
        if (ret) exitError("Could not compile regex");

        while (fgets (buf, sizeof(buf), shadow_file)) {
                ret = regexec(&regex, buf, 0, NULL, 0);
                if(ret == REG_NOMATCH) {
                        printf("No match: %s", buf);
                        exitError("Format error in Shadow file.");
                }

                token = strtok(buf, ":");
                strcpy(user, token);
                token = strtok(NULL, ":");
                if(token[0] == '$') {
                        begin_salt = token + 3;
                        end_salt = strchr(begin_salt, '$');
                        index = (size_t)(end_salt - begin_salt);

                        strncpy(salt, token, index + 3);

                        token = strtok(NULL, "$");
                        strcpy(hash, end_salt + 1);

                        insert_cell(list, user, salt, hash, NULL, NO_DEF);
                }
        }
        if (ferror(shadow_file))
                exitError("Error reading stdin\n");

        fclose(shadow_file);

        return list;
}
