#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/list.h"

struct list *new_list()
{
        struct list *list = (struct list *)calloc(1, sizeof(struct list));
        list->head = NULL;
        list->tail = NULL;
        list->nb_cell = 0;
        return list;
}

int insert_cell(struct list *l, char *u, char* s, char * h,
                char * p, criticity c)
{
        if(l == NULL) {
                // TODO Print error
                return 1;
        }
        if(u == NULL || s == NULL || h == NULL) {
                // TODO Print error
                return 1;
        }

        struct cell *cell       = (struct cell *)calloc(1, sizeof(struct cell));

        cell->username          = strdup(u);
        cell->hash              = strdup(h);
        cell->salt              = strdup(s);
        cell->password          = (p != NULL) ? strdup(p) : NULL;
        cell->criticity_lvl     = c;

        if(l->head == NULL) {
                l->head         = cell;
                l->tail         = cell;
        } else {
                l->tail->next   = cell;
                l->tail         = cell;
        }
        l->nb_cell              = l->nb_cell + 1;
        return 0;
}

void save_list(char* path)
{
        // TODO Save a list
}

struct list *load_list(char *path)
{
        // TODO Load list
        return new_list();
}

const char *criticity_to_string(criticity c)
{
        switch (c) {
        case VERY_CRITICAL:
                return "very critical";
        case CRITICAL:
                return "critical";
        case CAREFUL:
                return "careful";
        case GOOD:
                return "good";
        default:
                return "Unknwon";
        }
}

void print_list(struct list *list)
{
        printf("The list has %d cells :\n", list->nb_cell);

        struct cell *tmp = list->head;
        for (int i = 0; i < list->nb_cell; i++) {
                printf("|\n");
                printf("+- %s:%s$%s\n", tmp->username, tmp->salt, tmp->hash);
                printf("+- Password: %s, Criticity: %s.\n", tmp->password,
                       criticity_to_string(tmp->criticity_lvl));
                tmp = tmp->next;
        }
}

void destroy_list(struct list *l)
{
        struct cell *tmp = l->head;
        for (int i = 0; i < l->nb_cell; i++) {
                tmp = tmp->next;
                free(l->head->username);
                free(l->head->hash);
                free(l->head->salt);
                if(l->head->password != NULL)
                        free(l->head->password);
                free(l->head);
                l->head = tmp;
        }
        free(l);
}
