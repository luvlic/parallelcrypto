#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <argp.h>
#include <mpi.h>
#include "../include/parser.h"
#include "../include/list.h"
#include "../include/error.h"
#include "../include/hash.h"
#include "../include/dico.h"

const char *argp_program_version = "Shadow Lecter v0.1";
const char *argp_program_bug_address = "<contact@ludovic-boulay.com>";
static char doc[] = "Program to test password fiability.";
static char args_doc[] = "SHADOW_FILE";
static struct argp_option options[] = {
        { "verbose", 'v',      0, 0, "Produce verbose output" },
        { "measure", 't',      0, 0, "Show in ouput some measure of execution time" },
        { "not-display", 'n',      0, 0, "Not display the standard output" },
        { "backup-file", 'b', "FILE", 0, "Last backup."           },
        { 0 }
};

struct arguments {
        char *shadow_file;
        char *save_file;
        bool verbose;
        bool display;
        bool measure;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
        struct arguments *arguments = (struct arguments *)state->input;
        switch (key) {
        case 'b':
                arguments->save_file = arg;
                break;
        case 'v':
                arguments->verbose = true;
                break;
        case 't':
                arguments->measure = true;
                break;
        case 'n':
                arguments->display = false;
                break;
        case ARGP_KEY_ARG:
                if (state->arg_num >= 1) {
                        argp_usage(state);
                }
                arguments->shadow_file = arg;
                break;
        case ARGP_KEY_END:
                if (state->arg_num < 1)
                        argp_usage (state);                                     // Not enough arguments.
                break;
        default:
                return ARGP_ERR_UNKNOWN;
        }
        return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0 };

int my_rank;
int nb_proc;

void quit_slave_error()
{
        int quit = 0;
        for (int i = 1; i < nb_proc; i++) {
                MPI_Send(&quit, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        }
        MPI_Finalize();
        exitError("Abort.");
}

void master_function(struct arguments arguments)
{
        MPI_Status status;

        if(arguments.verbose) {
                printf("Backup file:\t\t\t%s\n", arguments.save_file);
                printf("Shadow file:\t\t\t%s\n", arguments.shadow_file);
                printf("Verbose:\t\t\t%s\n", arguments.verbose ? "yes" : "no");
        }

        fileExist(arguments.save_file);

        struct list *list= shadow_parser(arguments.shadow_file);
        if(arguments.display) {
            printf(GRN "[OK] Shadow file parsed successfully.\n" NRM);
            printf(GRN "[OK] %d users importated.\n", list->nb_cell);
        }
        if(arguments.verbose) print_list(list);

        struct list_dico *l = init_dictionaries("./dico/");
        if(l == NULL) {
                exitError("Error: there aren't any dictionaries in the dico repository.\n");
        }

        struct list_dico *tmp = l;
        if(arguments.verbose) {
                while(tmp != NULL) {
                        printf("Dictionary %s has %d words and his hash is %s.\n",
                               l->path, l->nb_words, l->hash);
                        tmp = tmp->next;
                }
                tmp = l;
        }

        // Get the name of the processor
        char processor_name[MPI_MAX_PROCESSOR_NAME];
        int name_len;
        MPI_Get_processor_name(processor_name, &name_len);



        // Check the dictionaries on all nodes
        while(tmp != NULL) {

                int message_size = 3 + strlen(tmp->path) + strlen(tmp->hash);

                // Allocate a buffer to hold the outcoming message
                int* message_buf = (int*)malloc(sizeof(int) * message_size);
                message_buf[0] = 1;
                message_buf[1] = strlen(tmp->path);
                message_buf[2] = strlen(tmp->hash);
                for (int i = 0; i < message_buf[1]; i++) {
                        message_buf[i + 3] = (int)tmp->path[i];
                }
                for (int i = 0; i < message_buf[2]; i++) {
                        message_buf[i + 3 + message_buf[1]] = (int)tmp->hash[i];
                }

                MPI_Request request_send[nb_proc];
                MPI_Status status_send[nb_proc];
                for (int i = 0; i < nb_proc - 1; i++) {
                        MPI_Isend(message_buf, message_size, MPI_INT, i + 1, 1,
                                               MPI_COMM_WORLD, &request_send[i]);
                        if(arguments.verbose) {
                                printf("Processor master send "
                                       "dictionary check command (%d) to %d/%d\n",
                                       message_buf[0], i + 1, nb_proc);
                        }
                }
                MPI_Waitall(nb_proc - 1, request_send, status_send);

                int nb_responses = 0;
                bool error = false;
                int response;
                while(nb_responses < nb_proc - 1) {
                        MPI_Recv(&response, 1, MPI_INT, MPI_ANY_SOURCE,
                                          MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                        if(arguments.verbose) {
                                printf("Processor master received response %s from slave %d/%d\n",
                                       response ? "true" : "false", status.MPI_SOURCE, nb_proc);
                        }
                        if(response == 0) {
                                char buf[100 + strlen(tmp->path)];
                                sprintf(buf, "Dictionary %s from slave %d "
                                        "is missing or incomplete.",
                                        tmp->path, status.MPI_SOURCE);
                                printError(buf);
                                error = true;
                        }
                        nb_responses++;
                }
                free(message_buf);
                if(error) quit_slave_error();
                tmp = tmp->next;
        }
        tmp = l;

        if(arguments.display) {
            printf(GRN "[OK] Dictionaries all checked.\n" NRM);
            printf(BLU "[INFO] Checking password of the users...\n" NRM);
        }

        double start = MPI_Wtime();

        struct cell *user = list->head;
        for (int i = 0; i < list->nb_cell; i++) {

                if(arguments.display)
                        printf(BLU "[INFO] Checking %s's password...\n" NRM, user->username);

                double start_user = MPI_Wtime();

                bool find = false;
                while(tmp != NULL) {
                        double start_dico = MPI_Wtime();
                        if(arguments.display)
                                printf(BLU "[INFO] Checking with %s...\n" NRM, tmp->path);

                        int message_size = 6 + strlen(user->hash)
                                             + strlen(tmp->path)
                                             + strlen(user->salt);

                        // Allocate a buffer to hold the outcoming message
                        int* message_buf = (int*)malloc(sizeof(int) * message_size);

                        int dico_range = tmp->nb_words / (nb_proc - 1);

                        // Message of type 2
                        // Test the hash password of an user.
                        // message_buf[0]           Type of the message.
                        // message_buf[1]           Length of the path string.
                        // message_buf[2]           First line of the dictionary where begin the attack.
                        // message_buf[3]           Last line of the dictionary where end the attack.
                        // message_buf[4]           Length of the hash string.
                        // message_buf[5]           Length of the salt string.
                        // message_buf[5..k]        Path of the dictionary to use.
                        // message_buf[k + 1..n]    Hash to test.
                        // message_buf[n + 1..end]  Salt of the user.
                        message_buf[0] = 2;
                        message_buf[1] = strlen(tmp->path);
                        message_buf[4] = strlen(user->hash);
                        message_buf[5] = strlen(user->salt);
                        for (int i = 0; i < message_buf[1]; i++) {
                                message_buf[i + 6] = (int)tmp->path[i];
                        }
                        for (int i = 0; i < message_buf[4]; i++) {
                                message_buf[i + 6 + message_buf[1]] = (int)user->hash[i];
                        }
                        for (int i = 0; i < message_buf[5]; i++) {
                                message_buf[i + 6 + message_buf[1] + message_buf[4]] = (int)user->salt[i];
                        }

                        MPI_Request request_send[nb_proc];
                        MPI_Status status_send[nb_proc];
                        for (int i = 0; i < nb_proc - 1; i++) {
                                message_buf[2] = 1 + i * dico_range;
                                message_buf[3] = (i + 1 == (nb_proc - 1)) ? tmp->nb_words : (i + 1) * dico_range;
                                MPI_Isend(message_buf, message_size, MPI_INT, i + 1, 2, MPI_COMM_WORLD, &request_send[i]);
                                if(arguments.verbose) {
                                        printf("Processor master send "
                                               "attack command (%d) to %d/%d\n",
                                               message_buf[0], i + 1, nb_proc);
                                }
                        }
                        MPI_Waitall(nb_proc - 1, request_send, status_send);

                        free(message_buf);
                        if(arguments.verbose) {
                                printf("\nProcessor master wait answers.\n\n");
                        }

                        int nb_responses = 0;
                        double start_trans_user = 0.0;
                        while(nb_responses < nb_proc - 1) {
                                int response_size;
                                // Probe for an incoming message from a slave
                                MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

                                // When probe returns, the status object has the size and other
                                // attributes of the incoming message. Get the message size
                                MPI_Get_count(&status, MPI_INT, &response_size);

                                // Allocate a buffer to hold the incoming message
                                int* response_buf = (int*)calloc(response_size, sizeof(int));

                                MPI_Recv(response_buf, response_size, MPI_INT, status.MPI_SOURCE, MPI_ANY_TAG,
                                         MPI_COMM_WORLD, &status);

                                if(arguments.verbose) {
                                        printf("Processor master received response %s from slave %d/%d\n",
                                               (response_size == 1) ? "notFind" : "find", status.MPI_SOURCE, nb_proc);
                                }
                                if(response_size != 1) {

                                        if(arguments.measure) {
                                                printf(YEL "[INFO] Find user = %1.4fs.\n" NRM,
                                                        MPI_Wtime() - start_dico);
                                        }
                                        start_trans_user = MPI_Wtime();

                                        char password[response_buf[1] + 1];
                                        for (int i = 0; i < response_buf[1]; i++) {
                                                password[i] = (char)response_buf[i + 2];
                                        }
                                        password[response_buf[1]] = '\0';
                                        if(arguments.display) {
                                                printf(BLU "[INFO] Slave %d find %s's password.\n" NRM,
                                                   status.MPI_SOURCE, user->username);
                                        }

                                        /*int stop = 3;
                                        MPI_Request request_send[nb_proc];
                                        MPI_Status status_send[nb_proc];
                                        for (int i = 0; i < nb_proc - 1; i++) {
                                                MPI_Isend(&stop, 1, MPI_INT, i + 1, 3, MPI_COMM_WORLD, &request_send[i]);
                                        }
                                        MPI_Waitall(nb_proc - 1, request_send, status_send);*/
                        		int stop = 3;
                        		for (int i = 1; i < nb_proc; i++) {
                                		MPI_Rsend(&stop, 1, MPI_INT, i, 3, MPI_COMM_WORLD);
                       		 	}


                                        user->password = strdup(password);
                                        find = true;
                                }
                                free(response_buf);
                                nb_responses++;
                        }

                        int stop = 3;
                        for (int i = 1; i < nb_proc; i++) {
                                MPI_Send(&stop, 1, MPI_INT, i, 3, MPI_COMM_WORLD);
                        }

                        if(find) {
                                if(arguments.measure) printf(YEL "[INFO] User transition = %1.4fs.\n" NRM, MPI_Wtime() - start_trans_user);
                                break;
                        } else
                                if(arguments.measure) printf(YEL "[INFO] Check dico %s = %1.4fs.\n" NRM, tmp->path , MPI_Wtime() - start_dico);

                        tmp = tmp->next;
                }

                if(arguments.measure) {
                        printf(YEL "[INFO] Check user %s = %1.4fs.\n" NRM,
                                    user->username, MPI_Wtime() - start_user);
                }

                tmp = l;
                user = user->next;
                find = false;
        }

        double end = MPI_Wtime();
        if(arguments.display || arguments.measure)
            printf(GRN "[OK] All users checked in %1.4fs.\n" NRM, end - start);

        int quit = 0;
        MPI_Request request_send[nb_proc];
        MPI_Status status_send[nb_proc];
        for (int i = 0; i < nb_proc - 1; i++) {
                MPI_Isend(&quit, 1, MPI_INT, i + 1, 0, MPI_COMM_WORLD, &request_send[i]);
                if(arguments.verbose) {
                        printf("Processor master send "
                               "quit command (%d) to %d\n", quit, i + 1);
                }
        }
        MPI_Waitall(nb_proc - 1, request_send, status_send);

        if(arguments.verbose) {
                printf(BLU "\n[INFO] THE END\n" NRM);
                print_list(list);
        }
}

void slave_function(bool verbose)
{
        char processor_name[MPI_MAX_PROCESSOR_NAME];
        int name_len;
        MPI_Get_processor_name(processor_name, &name_len);

        bool is_working = true;
        while(is_working) {
                MPI_Status status;
                int message_size;

                if(verbose) {
                        printf("Processor slave %s, rank %d waiting.\n",
                               processor_name, my_rank);
                }

                // Probe for an incoming message from process zero
                MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

                // When probe returns, the status object has the size and other
                // attributes of the incoming message. Get the message size
                MPI_Get_count(&status, MPI_INT, &message_size);

                // Allocate a buffer to hold the incoming message
                int* message_buf = (int*)malloc(sizeof(int) * message_size);

                // Now receive the message with the allocated buffer
                MPI_Recv(message_buf, message_size, MPI_INT, 0, MPI_ANY_TAG,
                         MPI_COMM_WORLD, &status);

                int type_message = status.MPI_TAG;
                if(verbose) {
                        printf("Processor slave %s, rank %d received command %d.\n",
                               processor_name, my_rank, type_message);
                }

                // Type 0: Quit.
                if(type_message == 0) {
                        is_working = false;
                        if(verbose) {
                                printf("Processor slave %s, rank %d received quit command.\n",
                                       processor_name, my_rank);
                        }
                }
                // Type 1: Ask to check if a dictionary is present.
                else if(type_message == 1) {
                        char dico_path[message_buf[1] + 1];
                        char dico_hash[message_buf[2] + 1];
                        for (int i = 0; i < message_buf[1]; i++) {
                                dico_path[i] = (char)message_buf[i + 3];
                        }
                        dico_path[message_buf[1]] = '\0';
                        for (int i = 0; i < message_buf[2]; i++) {
                                dico_hash[i] = (char)message_buf[i + 3 + message_buf[1]];
                        }
                        dico_hash[message_buf[2]] = '\0';

                        if(verbose) {
                                printf("Processor slave %s, rank %d received command check "
                                       "dictionary %s (%d) with the hash %s (%d).\n",
                                       processor_name, my_rank, dico_path, message_buf[1], dico_hash, message_buf[2]);
                        }

                        int check = check_dico(dico_path, dico_hash);
                        MPI_Send(&check, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);

                        if(verbose) {
                                printf("Processor slave %s, rank %d send response %s.\n",
                                       processor_name, my_rank, check ? "true" : "false");
                        }

                }
                // Type 2: Ask to test user password.
                else if(type_message == 2) {

                        int dico_start = message_buf[2];
                        int dico_end   = message_buf[3];
                        char dico_path[message_buf[1] + 1];
                        char user_hash[message_buf[4] + 1];
                        char user_salt[message_buf[5] + 1];
                        for (int i = 0; i < message_buf[1]; i++) {
                                dico_path[i] = (char)message_buf[i + 6];
                        }
                        dico_path[message_buf[1]] = '\0';
                        for (int i = 0; i < message_buf[4]; i++) {
                                user_hash[i] = (char)message_buf[i + 6 + message_buf[1]];
                        }
                        user_hash[message_buf[4]] = '\0';
                        for (int i = 0; i < message_buf[5]; i++) {
                                user_salt[i] = (char)message_buf[i + 6 + message_buf[1] + message_buf[4]];
                        }
                        user_salt[message_buf[5]] = '\0';

                        if(verbose) {
                                printf("Processor slave %s, rank %d received command test "
                                       "user hash %s$%s with the dictionary %s from %d to %d.\n",
                                       processor_name, my_rank, user_salt, user_hash, dico_path, dico_start, dico_end);
                        }

                        char * line = NULL;
                        size_t len = 0;
                        FILE * dico = fopen(dico_path, "r");
                        if (dico == NULL)
                                exitError("Error reading dictionary");

                        for (int i = 1; i < dico_start; i++) {
                                getline(&line, &len, dico);
                        }

                        char *res = NULL;
                        int find = 0;

                        int flag = 0;
                        int data;
                        MPI_Request mpi_request;
                        MPI_Irecv(&data, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &mpi_request);
                        MPI_Test(&mpi_request, &flag, &status);

                        for (int i = dico_start; i <= dico_end; i++) {

                                MPI_Test(&mpi_request, &flag, &status);
                                if(flag) {
                                        if(verbose) {
                                                printf("Processor slave %s, rank %d received quit command.\n",
                                                       processor_name, my_rank);
                                        }
                                        break;
                                }

                                getline(&line, &len, dico);
                                line[strlen(line) - 1] = '\0';

                                res = hash(line, user_salt);
                                find = compare_hash(res + 12, user_hash);
                                if(find) {
                                        if(verbose) {
                                                printf("Processor slave %s, rank %d find the password of the user: %s.\n",processor_name, my_rank, line);
                                        }
                                        int response_size = 2 + strlen(line);

                                        // Allocate a buffer to hold the outcoming message
                                        int* response_buf = (int*)malloc(sizeof(int) * response_size);
                                        response_buf[0] = 1;
                                        response_buf[1] = strlen(line);
                                        for (int i = 0; i < response_buf[1]; i++) {
                                                response_buf[i + 2] = (int)line[i];
                                        }

                                        MPI_Send(response_buf, response_size, MPI_INT, 0, 0, MPI_COMM_WORLD);

                                        if(verbose) {
                                                printf("Processor slave %s, rank %d send response true.\n",
                                                       processor_name, my_rank);
                                        }
                                        free(response_buf);
                                        break;
                                }
                                //free(res);
                        }

                        if(!find) {
                                MPI_Send(&find, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
                                if(verbose) {
                                        printf("Processor slave %s, rank %d send response false.\n",
                                               processor_name, my_rank);
                                }
                        }

                        fclose(dico);
                        if (line)
                                free(line);
                }
                // Type 3: Stop.
                if(type_message == 3) {
                        // Already stop
                        NULL;
                }

                free(message_buf);
        }

        if(verbose) {
                printf("Processor slave %s, rank %d quiting...\n",
                       processor_name, my_rank);
        }
}

int main(int argc, char *argv[]) {

        struct arguments arguments;
        arguments.save_file = NULL;
        arguments.shadow_file = NULL;
        arguments.verbose = false;
        arguments.display = true;
        arguments.measure = false;
        argp_parse(&argp, argc, argv, 0, 0, &arguments);

        // Initialize the MPI environment
        MPI_Init(NULL, NULL);
        // Get the number of processes
        MPI_Comm_size(MPI_COMM_WORLD, &nb_proc);

        // Get the rank of the process
        MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

        if(my_rank == 0)
                master_function(arguments);
        else
                slave_function(arguments.verbose);

        MPI_Finalize();
        return 0;
}
