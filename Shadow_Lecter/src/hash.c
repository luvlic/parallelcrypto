#define _XOPEN_SOURCE       /* See feature_test_macros(7) */
#include <stdio.h>
#include <string.h>
#include <unistd.h>

char *hash(char *password, char *salt)
{
        /* Hashed form of "GNU libc manual". */
        //const char *const pass = "$6$oNreSxEk$gCLoL7nKNSS2VQHc2AKhyoHtRfRRfsX85uSiXXlr805qdGOuCVoZ63aC/4Yx6.Lsz5YbBN3RrW3P9DefE4sol.";
        return crypt(password, salt);
}

int compare_hash(char *hash1, char *hash2)
{
    for (size_t i = 0; i < 33; i++) {
        if(hash1[i] != hash2[i]) return 0;
    }

    return 1;
}
