library(data.table)
library(ggplot2)
data = read.csv("output.csv",header=FALSE,sep=";")
total_exec = setNames(data[,c("V1","V2","V20")], c("iter", "procs", "time"))
m_tot_exec = aggregate(total_exec[, 3], list(total_exec$procs), mean)
s_tot_exec = aggregate(total_exec[, 3], list(total_exec$procs), sd)
total_exec = setNames(cbind(m_tot_exec, s_tot_exec$x), c("NumberOfProcessor", "Time", "Error"))
ggplot(total_exec, aes(x=NumberOfProcessor, y=Time)) +
geom_errorbar(aes(ymin=Time-Error, ymax=Time+Error), width=.8) +
geom_point()
