#!/usr/bin/python
import csv

measures = [[0 for x in range(20)] for y in range(10 * 11)]

index = 0
for i in range(0, 10):
    for value in [12, 23, 24, 30, 35, 46, 47, 50, 53, 57, 69]:
        measures[index][0] = i
        measures[index][1] = value
        path = 'results/' + str(i) + '_' + str(value) + '.txt'
        with open(path, 'r') as f:
            for l in range(0, 18):
                line = f.readline()
                time = (line.split())[-1]
                measures[index][2 + l] = time[:6]
        index += 1


with open("output.csv", "wb") as f:
    writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
    writer.writerows(measures)
