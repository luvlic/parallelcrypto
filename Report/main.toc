\select@language {english}
\select@language {brazilian}
\select@language {english}
\contentsline {chapter}{1~Intel AES-NI for brute force attack}{8}{CHAPTER.1}
\contentsline {section}{\numberline {1.1}Introduction}{8}{section.1.1}
\contentsline {section}{\numberline {1.2}Results}{8}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Performance using the Intel AES-NI}{8}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Distributed brute force attack using AES-NI}{9}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Conclusion}{10}{section.1.3}
\contentsline {chapter}{2~Distributed dictionary attack}{11}{CHAPTER.2}
\contentsline {section}{\numberline {2.1}Introduction}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}The shadow file}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}The program - Shadow Lecter}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}The parser}{14}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}The dictionaries}{14}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}The attack}{14}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Results}{15}{section.2.4}
\contentsline {section}{\numberline {2.5}Improvements}{17}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Save file}{17}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Buffer}{18}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Rule-based Attack}{18}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}OpenMP}{19}{subsection.2.5.4}
\contentsline {section}{\numberline {2.6}Conclusion}{19}{section.2.6}
